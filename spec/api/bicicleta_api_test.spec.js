var mongoose = require('mongoose');
var Bicicleta = require('../../models/bicicleta');
var request = require('request');
 var server = require('../../bin/www');
 const { base } = require('../../models/bicicleta');

var base_url = 'http://localhost:3000/api/bicicletas';

describe("Bicicleta API",()=>{
    beforeEach(function(done){
        var mongoDB = 'mongodb://localhost/red_bicicletas';
        mongoose.connect(mongoDB, { useNewUrlParser: true, useUnifiedTopology:true} );

        const db = mongoose.connection;
        db.on('error', console.error.bind(console, 'connection error...'));
        db.once('open', function(){
            console.log('Nos conectamos a la base de datos');
            done();
        });
    });


    afterEach(function(done){
        Bicicleta.deleteMany({}, function(err, success){
            if(err) console.log(err);
            mongoose.disconnect(err); 
            done();
        });
    });

    describe("GET Bicicletas /", ()=>{
        it("Status 200", (done)=>{       
           request.get(base_url, function(error, response,body){
               var result = JSON.stringify(body);
               console.log("DATOOOS: ", body)
                expect(response.statusCode).toBe(200);
                expect(result.bicicletas.length).toBe(0);
                done();
           }); 
        });
    });
    

    describe("POST Bicicletas /create", ()=>{
        it("Status 200", (done)=>{
            var headers = {"content-type":"application/json"};
            var aBici = '{"id": 3,"color":"blanco","modelo":"atx","lat":"-25.3524","lng":"-57.6234"}';       
            request.post({
               headers:  headers,
               url:     base_url + '/create',
               body:     aBici
                
            }, function(error, response,body){
                 expect(response.statusCode).toBe(200);
                 var bici = JSON.stringify(body).bicicleta;
                 console.log("BICIIIII: ", bici);
                 expect(bici.color).toBe("blanco");
                 expect(bici.ubicacion[0]).toEqual(-25.3524);
                 expect(bici.ubicacion[1]).toEqual(-57.6234);
                 done();
            });
        });
    });



});
/*
beforeEach(()=>{
    console.log('testeando...');
});
describe('Bicicleta API', ()=>{
    describe('GET BICI /', ()=>{
        it('Status 200', ()=>{
           expect(Bicicleta.allBicis.length).toBe(0);

           var a = new Bicicleta(1,'blanco','mb',[-25.3524,-57.6234]);
           Bicicleta.add(a);

           request.get('http://localhost:3000/api/bicicletas', function(error, response,body){
                expect(response.statusCode).toBe('200');
           }); 
        });
    });

    describe('POST BICI /create', ()=>{
        it('Status 200', (done)=>{
            var headers = {'content-type':'application/json'};
            var aBici = {"id":8,"color":"blanco","modelo":"atx","lat":"-25","lng":"-57"}          
            request.post({
               headers:  headers,
               url:     'http://localhost:3000/api/bicicletas/create',
               body:     aBici
                
            }, function(error, response,body){
                 expect(response.statusCode).toBe('200');
                 expect(Bicicleta.findById(8).color).toBe('blanco');
                 done();
            });
        });
    });
});

*/