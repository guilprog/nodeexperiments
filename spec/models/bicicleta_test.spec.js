var mongoose = require('mongoose');
//const bicicleta = require('../../models/bicicleta');
var Bicicleta = require('../../models/bicicleta');

describe('Testing Bicis', function(){
    beforeEach(function(done){
        var mongoDB = 'mongodb://localhost/red_bicicletas';
        mongoose.connect(mongoDB, { useNewUrlParser: true,  useUnifiedTopology:true} );

        const db = mongoose.connection;
        db.on('error', console.error.bind(console, 'connection error...'));
        db.once('open', function(){
            console.log('Nos conectamos a la base de datos');
            done();
        });
    });
    afterEach(function(done){
        Bicicleta.deleteMany({}, function(err, success){
            if(err) console.log(err);
            
            done();
            mongoose.disconnect(err); 
        });
    });

    describe('Bicicleta.createInstance',()=>{
        it('crea una instancia de bicicleta', ()=>{
            var bici = Bicicleta.createInstance(1,'verde','atx',[-35.5,-57.1]);

            expect(bici.code).toBe(1);
            expect(bici.color).toBe('verde');
            expect(bici.modelo).toBe('atx');
            expect(bici.ubicacion[0]).toEqual(-35.5);
            expect(bici.ubicacion[1]).toEqual(-57.1);
           
        })
    });

    describe('Bicicleta.allBicis',()=>{
        it('inicia vacia', (done)=>{
            Bicicleta.allBicis(function(err, bicis){
                expect(bicis.length).toBe(0);
                done();
            });
            
        });
       
    });
    describe('Bicicleta.add',()=>{
        it('agrega bici', (done)=>{
            var aBici = new Bicicleta({code:1,color:'rojo',modelo:'bmx'});
            Bicicleta.add(aBici, function(err, newBic){
                if(err) console.log(err);
                Bicicleta.allBicis(function(err, bicis){
                    expect(bicis.length).toBe(1);
                    expect(bicis[0].code).toEqual(aBici.code);

                    done();
                }); 
            });
           
            
        });
       
    });
  

    describe('Bicicleta.findByCode',()=>{
        it('debe encontrar la bici con code 1', (done)=>{
            Bicicleta.allBicis(function(err,bicis){
                expect(bicis.length).toBe(0);

                var aBici = new Bicicleta({code:1,color:'rojo',modelo:'bmx'});
                Bicicleta.add(aBici, function(err, newBici){
                    if(err) console.log(err);

                    var aBici2 = new Bicicleta({code:2,color:'blanco',modelo:'atx'});
                    Bicicleta.add(aBici2, function(err, newBici){
                        if(err) console.log(err);
                        Bicicleta.findByCode(1, function(error, targetBici){
                            expect(targetBici.code).toBe(aBici.code);
                            expect(targetBici.color).toBe(aBici.color);
                            expect(targetBici.modelo).toBe(aBici.modelo);

                            done();
                        });
                    });
                });
            });

        });
    });

});

/*
beforeEach(()=>{
    Bicicleta.allBicis = [];
});
describe('Bicicleta.allBicis', ()=>{
    it('inicia vacio', ()=>{
        expect(Bicicleta.allBicis.length).toBe(0);
    });

});

describe('Bicicleta.Add', () =>{
    it('se agrega una', ()=>{
        expect(Bicicleta.allBicis.length).toBe(0);

        var a = new Bicicleta(1,'azul','MB',[-25.3524,-57.6245]);
        Bicicleta.add(a);

        expect(Bicicleta.allBicis.length).toBe(1);
        expect(Bicicleta.allBicis[0]).toBe(a);

    });
});

describe('Bicicleta.FindById',()=>{
    it('debe devolver bici id 1', ()=>{
        expect(Bicicleta.allBicis.length).toBe(0);

        var a = new Bicicleta(1,'purpura','MB'); 
        Bicicleta.add(a);
        var b = new Bicicleta(2,'azul','atx'); 
        Bicicleta.add(b);

        var targetBici = Bicicleta.findById(2);
        expect(targetBici.id).toBe(2);
        expect(targetBici.color).toBe(b.color);
        expect(targetBici.modelo).toBe(b.modelo);
    });

});*/
