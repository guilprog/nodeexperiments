var mongoose = require('mongoose');
//const { model } = require('./bicicleta');
const uniqueValidator = require('mongoose-unique-validator');
var Reserva = require('./reserva');
var bcrypt = require('bcrypt');
const crypto = require('crypto');
var Token = require('../models/token'); //require('./token');
var mailer = require('../mailer/mailer');
const saltRounds = 10;var Schema = mongoose.Schema;

const validateEmail = function (email) {
    const regExp = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,4})+$/;
    return regExp.test(email);
}

var usuarioSchema = new Schema({
    nombre: {
        type: String,
        trim: true,
        required: [true, 'Ingrese nombre']
    },
    email: {
        type: String,
        trim: true,
        required: [true, 'Ingrese email'],
        lowercase: true,
        unique:true,
        validate:[validateEmail, 'Favor ingrese un email valido'],
        match: [/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,4})+$/] //se corre a nivel mongo
    },
    password: {
        type: String,
        required: [true, 'El password es obligatorio']
    },
    passwordResetToken: String,
    passwordResetTokenExpires: Date, //token expiracion
    verificado: {
        type: Boolean,
        default: false 
    },
    googleId: String,
    facebookId: String
   
});
usuarioSchema.plugin(uniqueValidator, {message: 'El {PATH} ya existe con otro usuario`'});
usuarioSchema.pre('save', function(next){
    if ( this.isModified('password') ){
        this.password = bcrypt.hashSync(this.password, saltRounds);
    }
    next();
});

usuarioSchema.methods.validPassword = function(password) {
    return bcrypt.compareSync(password, this.password);
}

usuarioSchema.methods.reserva = function(biciId,desde, hasta,cb){
    var reserva = new Reserva({usuario:this._id, bicicleta:biciId, desde:desde, hasta:hasta});
    console.log(reserva);
    reserva.save(cb);
}

usuarioSchema.methods.enviar_email_bienvenida = function(cb) {
    const token = new Token({_userId: this.id, token: crypto.randomBytes(16).toString('hex')})
    const email_destination = this.email;
    token.save(function(err){
      if ( err ) { return console.log(err.message)}
      const mailOptions = {
        from: 'no-reply@redbicicletas.com',
        to: email_destination,
        subject: 'Verificacion de cuenta',
        text: 'Hola,\n\n' + 'Por favor, para verificar su cuenta haga click en este link: \n' + 'http://localhost:3000' + '\/token/confirmation\/' + token.token + '\n'
      }
  
      mailer.sendMail(mailOptions, function(err){
        if( err ) { return console.log(err.message) } 
        console.log('Se ha enviado un email de bienvenida a: ' + email_destination)
      })
    })
}

usuarioSchema.methods.resetPassword =  function(cb){
    const token = new Token({_userId: this.id, token: crypto.randomBytes(16).toString('hex')});
    const email_destination = this.email;
     token.save(function (err) {
      if (err) {return cb(err)}
      const mailOptions = {
        from: 'bicicletajonas@gmail.com',
        to: email_destination,
        subject: 'Reseteo de password de cuenta',
        text: 'Hola,\n\n' 
        + 'Por favor, para resetar el password de su cuenta haga click en este link: \n' 
        + 'http://localhost:3000'
        + '\/resetPassword\/' + token.token + '\n'
      }
       mailer.sendMail(mailOptions, function(err){
        if( err ) { return cb(err) } 
        console.log('Se ha enviado un email para resetar el password a: ' + email_destination)
      })
  
      cb(null);
  
    })
  }


  usuarioSchema.statics.findOneOrCreateByGoogle = function findOneOrCreate(condition, callback) {
    const self = this;
    console.log(condition);

    this.findOne({
        $or: [
            { 'googleId': condition.id },
            { 'email': condition.emails[0].value }
        ]
    }, 
    (err, result) => {
        if (result) {
            callback(err, result);
        } else {
            let values = {};
            console.log('=============== CONDITION ===============');
            console.log(condition);

            values.googleId = condition.id;
            values.email = condition.emails[0].value;
            values.nombre = condition.displayName || 'SIN NOMBRE';
            values.verificado = true;
            values.password = crypto.randomBytes(16).toString('hex');

            console.log('=============== VALUES ===============');
            console.log(values);

            self.create(values, function (err, user) {
                if (err) {
                    console.log(err);
                }

                return callback(err, user);
            });
        }
    });
}

usuarioSchema.statics.findOneOrCreateByFacebook = function findOneOrCreate(condition, callback) {
  const self = this;
  console.log(condition);

  this.findOne({
      $or: [
          { 'facebookId': condition.id },
          { 'email': condition.emails[0].value }
      ]
  }, 
  (err, result) => {
      if (result) {
          callback(err, result);
      } else {
          let values = {};
          console.log('=============== CONDITION ===============');
          console.log(condition);

          values.facebookId = condition.id;
          values.email = condition.emails[0].value;
          values.nombre = condition.displayName || 'SIN NOMBRE';
          values.verificado = true;
          values.password = crypto.randomBytes(16).toString('hex');

          console.log('=============== VALUES ===============');
          console.log(values);

          self.create(values, function (err, user) {
              if (err) {
                  console.log(err);
              }

              return callback(err, user);
          });
      }
  });
}


module.exports = mongoose.model('Usuario', usuarioSchema)