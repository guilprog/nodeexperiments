var map = L.map('main_map').setView([-25.3524,-57.6234],16); 
L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    attribution: '&copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors'
    //tileSize: 512

}).addTo(map);



$.ajax({
    dataType: "json",
    url:"api/bicicletas",

    success:function(result){
        console.log(result);
        result.bicicletas.forEach(function(bici) {
            L.marker(bici.ubicacion, {title:bici.id}).addTo(map);
        });
    },
    failure: function(error){
        console.log(error);   
    }


});